import { IsEmpty, MinLength, Matches } from 'class-validator';
import { IsNotEmpty } from 'class-validator/types/decorator/decorators';
export class CreateUserDto {
  @IsNotEmpty()
  @MinLength(5)
  login: string;

  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  @MinLength(8)
  @IsNotEmpty()
  password: string;
}

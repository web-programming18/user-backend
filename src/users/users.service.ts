import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { notDeepEqual } from 'assert';
const users: User[] = [
  { id: 1, login: 'admin', name: 'administeator', password: 'Pass@123' },
  { id: 2, login: 'user1', name: 'User 1', password: 'Pass@123' },
  { id: 3, login: 'user2', name: 'User 2', password: 'Pass@123' },
];
let lastUserId = 4;
@Injectable()
export class UsersService {
  create(createUserDto: CreateUserDto) {
    const newUser: User = {
      id: lastUserId++,
      ...createUserDto, //createUserDto => login, name, password
    };

    users.push(newUser);
    return newUser;
  }

  findAll() {
    return users;
  }

  findOne(id: number) {
    const index = users.findIndex((userIndex) => {
      return userIndex.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return users[index];
  }

  update(id: number, updateUserDto: UpdateUserDto) {
    const index = users.findIndex((userIndex) => {
      return userIndex.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    // console.log('user' + JSON.stringify(users[index]));
    // console.log('update' + JSON.stringify(updateUserDto));
    const updateUser: User = {
      ...users[index],
      ...updateUserDto,
    };
    users[index] = updateUser;
    return updateUser;
  }

  remove(id: number) {
    const index = users.findIndex((userIndex) => {
      return userIndex.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedUser = users[index];
    users.splice(index, 1);
    return deletedUser;
  }
  reset() {
    const users = [
      { id: 1, login: 'admin', name: 'administeator', password: 'Pass@123' },
      { id: 2, login: 'user1', name: 'User 1', password: 'Pass@123' },
      { id: 3, login: 'user2', name: 'User 2', password: 'Pass@123' },
    ];
    lastUserId = 4;
    return 'RESET';
  }
}
